<script>
	//------------------------- Extension Overaly Script ---------------------------

    // Injects js to webpage
    scriptInjector = link =>{
        var scriptTag = document.createElement('script');
        scriptTag.type = 'text/javascript';
        scriptTag.src = link;
        (document.head || document.documentElement).appendChild(scriptTag);
    }

	// Include Hash.js 
	scriptInjector('https://cdn.hashingsystems.com/hash.js');

    // Initialize variables
    var url = window.location.href;

    // Sensor that can be attached to div to sense the dimension changes
    divResizeSensor = (element, callback) =>{
        let zIndex = parseInt(getComputedStyle(element));
        if(isNaN(zIndex)) { zIndex = 0; };
        zIndex--;

        let expand = document.createElement('div');
        expand.style.cssText="position:absolute;left:0px;top:0px;right:0px;bottom:0px;overflow:hidden;visibility:hidden";
        expand.style.zIndex = zIndex;

        let expandChild = document.createElement('div');
        expandChild.style.cssText = "position:absolute;left:0px;top:0px;width:10000000px;height:10000000px";
        expand.appendChild(expandChild);

        let shrink = document.createElement('div');
        shrink.style.cssText="position:absolute;left:0px;top:0px;right:0px;bottom:0px;overflow:hidden;visibility:hidden";
        shrink.style.zIndex = zIndex;

        let shrinkChild = document.createElement('div');
        shrinkChild.style.cssText="position:absolute;left:0px;top:0px;width:200%;height:200%";
        shrink.appendChild(shrinkChild);

        element.appendChild(expand);
        element.appendChild(shrink);

        function setScroll()
        {
            expand.scrollLeft = 10000000;
            expand.scrollTop = 10000000;

            shrink.scrollLeft = 10000000;
            shrink.scrollTop = 10000000;
        };
        setScroll();

        let size = element.getBoundingClientRect();

        let currentWidth = size.width;
        let currentHeight = size.height;

        let onScroll = function()
        {
            let size = element.getBoundingClientRect();

            let newWidth = size.width;
            let newHeight = size.height;

            if(newWidth != currentWidth || newHeight != currentHeight)
            {
                currentWidth = newWidth;
                currentHeight = newHeight;

                callback();
            }

            setScroll();
        };

        expand.addEventListener('scroll', onScroll);
        shrink.addEventListener('scroll', onScroll);
    };

    // Retrieves cookie information on the given key
    getCookie = cname => {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Decides if Notifier UI should be injected based on user login
    checkToNotify = path => {
        var isUser = isLoggedIn();
        var loginUser = document.querySelector('.ui.avatar.image');
        var profileUser=document.querySelector('.ui.circular.image');
        if (loginUser && profileUser && loginUser.src===profileUser.src) {
            isUser = true;
        }
        else {
            isUser = false;
        }
        if (path.includes('dailystamp.io/user/') && isUser) {
            var badgeElement = document.querySelector('.text-badge');
            var notifier = document.querySelector('.notifier');
            if (!badgeElement && !notifier) {
                renderInstructPrompt();
            }
        } else {
            clearNotifier();
        }
    }

    // Check to decide if tipping button should be injected
    checkToTip = path => {
        
       // var isUser = isLoggedIn(); // If user login is to be checked
        if(path==="https://dailystamp.io/"){
            injectTippingButtons('home');
            const postContainer = document.querySelector(".nine.wide.computer.twelve.wide.tablet.column");
            divResizeSensor(postContainer, ()=>{
                injectTippingButtons('home');
            });
            const postTypeSelector = document.querySelector(".menu.transition.visible");
        }else if(path.includes("https://dailystamp.io/post/")){
            injectTippingButtons('post');
        }else if(path.includes("https://dailystamp.io/question/")){
            injectTippingButtons('question');
        }
    }

    // Checks if user is any user is logged in and a session is on
    isLoggedIn=()=>{
        return getCookie('userId') && getCookie('userId') !== '' ? getCookie('userId') : false;
    }

    // Removes the instruction Notifier
    clearNotifier = () => {
        var notifier = document.querySelectorAll('.notifier');
        Array.from(notifier).forEach(function (element) {
            element.parentNode.removeChild(element);
        });
    }

    // Removes Connect button from UI
    clearConnectButton = () => {
        var connectButton = document.querySelector('#connect-account');
        connectButton.parentNode.removeChild(connectButton);
    }
 
    // Renders Connect Button into the UI
    injectConnectButton = () => {
        //Check if Edit Profile modal is open and then inject connect button
        var modalParent = document.querySelector('.ui.scrolling.modal.transition.visible.active');
        aboutInput = document.querySelectorAll(".ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p");
        aboutInput = aboutInput[aboutInput.length - 1];
        var saveButton = document.querySelector('.ui.positive.button');
        var hashResponse = null;
        if (modalParent && document.querySelector('#notifier-pill')) {
            var modalHeaderParent = modalParent.querySelector('.header');
            connectButton = document.createElement('button');
            connectButton.setAttribute('id', 'connect-account');
            connectButton.classList.add('disabled');
            connectButton.innerHTML = 'Checking';

            modalHeaderParent.appendChild(connectButton);
            if (window.hash && window.hash.triggerCheckBalance) {
                window.hash.triggerCheckBalance('', (err, res) => {
                    console.log('ERROR::: paul tes', err, res)
                    if (err) {
                        connectButton.classList.add('warn');
                        connectButton.classList.remove('disabled');
                        connectButton.innerHTML = 'No acounts found on extension';
                    } else {
                        hashResponse = res;
                        if (res.currentNetwork === 'mainnet') {
                            connectButton.classList.remove('warn');
                            connectButton.classList.remove('disabled');
                            connectButton.innerHTML = `Connect Wallet (${res.currentAccount})`;

                        } else {
                            connectButton.classList.add('warn');
                            connectButton.classList.remove('disabled');
                            connectButton.innerHTML = 'Account is not on mainnet';
                        }
                    }
                })

            } else {
                connectButton.classList.add('warn');
                connectButton.classList.remove('disabled');
                connectButton.innerHTML = 'Cannot connect Extension';
            }

            connectButton.addEventListener("click", function () {
                if (hashResponse && hashResponse.currentNetwork == 'mainnet' && hashResponse.currentAccount) {
                    aboutInput.innerHTML = `${aboutInput.innerHTML}@$%ℏ${hashResponse.currentAccount}@$%ℏ`;
                    console.log('HTML paul', aboutInput.innerHTML)
                    connectButton.innerHTML = 'Connecting...'
                    setTimeout(() => {
                        clearNotifier();
                        clearConnectButton();
                        saveButton.click();
                    }, 900);
                }
            });
        }
    }

    // Render Instruction/guide to connect or associate current profile to account-id
    renderInstructPrompt = () => {

        // Injecting Styles for all new elements
        var css = '#notifier-pill{padding:10px;margin:-15px auto 10px auto;text-align:center;font-size: 12px;background:#fff0227d;width:80%;border-radius:7px} #connect-account{border: 1px solid #49d0ab;background: transparent; border-radius: 15px;padding: 8px 12px;font-size: 12px;margin-left: 10px;cursor:pointer;outline:none;} #connect-account.disabled{cursor:default!important;pointer-events:none;border-color:#8e8d8a;background:#d0cecb!important;} #connect-account.warn{cursor:default!important;pointer-events:none;border-color:#f5ac09;background:#f3cf8b!important;} #connect-account:hover{background:#daf9eb;}';
        var style = document.createElement('style');
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(style);

        // Fetching Elements from page and store in variables
        parent = document.querySelector('.main-wrapper');
        child = document.querySelector('.ui.container.fluid');

        // Create Notice element
        notifyTag = document.createElement('div');
        notifyTag.setAttribute('id', 'notifier-pill');
        notifyTag.setAttribute('class', 'notifier');
        notifyTag.innerHTML = 'To connect your wallet just go to Edit Profile section by clicking “Edit Profile” button and thereafter click on “Connect Wallet" button';

        // Parent adopting new child
        child.parentNode.removeChild(child);
        parent.appendChild(notifyTag);
        parent.appendChild(child);

        var buttons = document.querySelectorAll('.ui.basic.circular.right.floated.button')
        if (buttons && buttons.length > 0) {
            buttons.forEach((btn, i) => {
                if (btn.innerText === 'Edit Profile') {
                    btn.addEventListener("click", function () {
                        setTimeout(() => {
                            injectConnectButton()
                        }, 1)
                    });
                }
            })
        }
    }

    // Renders Tiping button into the UI
    injectTippingButtons = async(page) => {
        let allPosts =[];
        if(page==='home'){
            const posts = document.querySelectorAll(".card.post-item.post-list-item");
            const answerPosts = document.querySelectorAll(".card.post-item.answer-list-item")
            const questionPosts = document.querySelectorAll(".card.question-item.question-list-item")
            // const allPosts = document.querySelectorAll(".ui.fluid.card.post-item"); // All posts
            allPosts = [...posts,...answerPosts,...questionPosts];
        }else if(page==='post'){
            const posts = document.querySelectorAll(".ui.fluid.card.post-item");
            allPosts = [...posts];
        }else if(page==='question'){
            const answerPosts = document.querySelectorAll(".ui.fluid.card.post-item.answer-list-item")
            const questionPosts = document.querySelectorAll(".ui.fluid.card.question-item");
            allPosts = [...questionPosts,...answerPosts];
        }
       
        let postCount=0;

        for(const post of allPosts){
            
            let type='';
            if(page==='home' || page==='post'){
                type=getPostType(post);
            }else if(page==='question'){
                if(postCount===0){
                    type='question';
                }else{
                    type='answer';
                }
            }
            const userHeader = post.querySelector('.header.user-bar-name');
            if(userHeader){
                const badge=userHeader.querySelector(".text-badge.text-badge-silver"); 
                const userHasAccountId = badge && badge.innerHTML && isAccount(badge.innerText);
                if(userHasAccountId){
                    const buttonCollection = post.querySelector('.ui.large.horizontal.link.list.card-footer-left-wrapper');
                    const hasTipButton=buttonCollection.querySelector('.tip-btn');
                    const postId=createUniquePostId(type,post);
                   
                    if(!hasTipButton && postId){
                        renderTipButton(badge.innerHTML,buttonCollection,postId);
                        await renderTipEarned(badge.innerHTML,buttonCollection,postId);
                    }
                }
            }
            postCount ++;
        }
    }

    getPostType = post => {
        return post.querySelector(".content.card-head").querySelector(".card-head-content-type").innerText.toLowerCase().trim();
    }

    createUniquePostId = (type,post) => {
        const postLinkArray=post.querySelectorAll('a');
        let link='';
        let id=''
        switch(type){
            case 'post':
                link = postLinkArray[4].pathname;
                id= `${link.split('/')[1]}-${link.split('/')[2]}`;
                break;
            
            case 'answer':
                id=post.querySelector('.link-placeholder').id;
                if(!id){
                    link = postLinkArray[3].hash;
                    id = `${link.substring(1)}`;
                }
                break;

            case 'question':
                if(postLinkArray[3] && postLinkArray[3].pathname){
                    link =  postLinkArray[3].pathname;
                }else{
                    link = window.location.pathname;
                    link = link.split('#')[0];
                }
                id= `${link.split('/')[1]}-${link.split('/')[2]}`;
                break;
        }
        return id;
    }

    // Renders a single tip earned text
    renderTipEarned = async(accountId,parent,postId) =>{
        if(postId){
            let amountEarned=0;
            const url = `https://vj47hfreuj.execute-api.us-east-1.amazonaws.com/prod?memo=${postId}&transactionTypes=CRYPTO_TRANSFER`;  
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", url, false );
            xmlHttp.setRequestHeader("Content-Type","application/json") // false for synchronous request
            xmlHttp.send( null );
            let res = JSON.parse(xmlHttp.response);
            let txs = res.body.data;
            txs=txs ? txs.filter((tx)=>tx.memo===`ds-pid-${postId}`):[];
            if(txs && txs.length>0){
                txs.forEach((tx)=>{
                    if(tx.transfers[tx.transfers.length-1].amount){
                        amountEarned+=tx.transfers[tx.transfers.length-1].amount;
                    }
                })
                if(amountEarned>0){
                    amountEarned=amountEarned/100000000
                }
            }

            let divElement=document.createElement('div');
            divElement.style.cssText="display: inline-block;margin-left: 15px;font-size: 13.5px;color: #57dc99;"
            divElement.innerHTML=`${(amountEarned).toFixed(8)} ħ`;
            parent.appendChild(divElement); 
        }
        
        return
    }

    // Renders a single tip button
    renderTipButton = (accountId,parent,postId) =>{
        
        let wrapperElement=document.createElement('div');
        wrapperElement.setAttribute('role','listitem');
        wrapperElement.setAttribute('class','item card-footer-left-item footer-like-button');
        
        let divElement=document.createElement('div');
        divElement.style.display='inline-block';
        divElement.innerHTML=`<button class="ui tiny basic button like-button tip-btn" onclick="tipUser('${accountId}','${postId}')"><span>Tip</span></button>`;
        
        let inputElement=document.createElement('input');
        inputElement.setAttribute('id',`tipInput-${postId}`);
        inputElement.setAttribute('type','text');
        inputElement.setAttribute('placeholder','Tipping amount ħ');
        inputElement.style.cssText="margin-right: 10px;border-radius: 5px;height: 30px;border: 1px solid rgba(202, 200, 200, 0.8);width: 106px;font-size: 11px;padding-left: 10px;padding-right: 10px;outline:none;";

        wrapperElement.appendChild(inputElement);
        wrapperElement.appendChild(divElement);
        parent.appendChild(wrapperElement); 
    }

    notify=(message,status='info',delay=5)=>{
    // Custom Notifier Library
    // @Author PAUL LOBO
        if(typeof message==='string' && message.length>0){
            switch(status){
                case 'info':
                    status='#b8e6f5';
                    break;
                
                case 'success':
                    status='#9af7cd';
                    break;

                case 'error':
                    status='#f79ab3';
                    break;

                case 'warn':
                    status='#f5c28a';
                    break;
            }

            const notifyTag = document.createElement('div');
            notifyTag.style.cssText=`position:fixed;z-index: 999;background:${status};width: auto;padding: 5px;padding-left: 10px;padding-right: 10px;bottom: 8%;left: 50%;font-size: 13.5px;border-radius: 12px;box-shadow: blue;-webkit-box-shadow: 0px 3px 12px -5px rgba(0,0,0,0.75);-moz-box-shadow: 0px 3px 12px -5px rgba(0,0,0,0.75);box-shadow: 0px 3px 12px -5px rgba(0,0,0,0.75);-moz-box-shadow: 0px 3px 19px -6px rgba(0,0,0,0.75);-webkit-animation: notifyAnimation ${delay}s ;animation: notifyAnimation ${delay}s ;`;
            notifyTag.innerHTML=message;
            document.body.appendChild(notifyTag);
            
            setTimeout(() => {
                document.body.removeChild(notifyTag);
            }, delay*1000+1);
        }
    }

    tipUser= (accountId,postId) =>{
    if(postId){
        const hash=window.hash;
        const amount =Number(document.getElementById(`tipInput-${postId}`).value);
            if(amount && amount > 0 && !isNaN(amount) ){
                notify('Processing Tip...','info'); 
                if(hash && hash.triggerCheckBalance){
                    hash.triggerCheckBalance('',(err,res)=>{
                        if(!err){
                            let data = {
                                    memo: `ds-pid-${postId}`,
                                    recipientlist: `[{"tinybars": "${amount*100000000}", "to":"${accountId}"}]`
                                }
                            hash.triggerCryptoTransfer(data, (error,response)=>{
                                if(!error){
                                    if(response.nodePrecheckcode===22){
                                        notify(`User (${accountId}) is tipped successfully !`,'success');
                                    }
                                }else{
                                    if(error){
                                        notify(`User (${accountId}): ${error}`,'warn'); 
                                    }else{
                                        notify(`User (${accountId}) could not be tipped`,'warn');
                                    }
                                }
                            });
                        }else{
                            notify(res.message,'error'); 
                        }
                    })
                }else{
                    notify('Cannot Tip, Since Composer Hedera extension not Found','error'); 
                    console.log('Error::: Cannot Tip, Since Hash.js was not Found')
                }
            }else{
                notify('Invalid tipping amount','warn'); 
            }
        }
        else{
            notify(`Sorry, Post could not be identified`,'warn'); 
        }

    }

    // To Check if string is accountID
    isAccount=str=>{
        const accountIdPattern = /0.0.[0-9]{1,255}/g;
        return accountIdPattern.test(str);
    }

    window.addEventListener('load', () => {
            // Check once when page is loaded
            checkToNotify(url);
            // Check to add tipping Button
            checkToTip(url);
            // Will detect route changes happening internally by React
            ['click', 'popstate', 'onload'].forEach(evt =>
                window.addEventListener(evt, function () {
                    requestAnimationFrame(() => {
                        console.log('Checking Now PL 1',url)
                        if (url !== location.href) {
                            console.log('Checking Now PL 2')
                            checkToNotify(location.href);
                            checkToTip(location.href);
                        }
                        url = location.href;
                    });
                }, true)
            );
        })

//---------------------------------------------------------------------------------
</script>